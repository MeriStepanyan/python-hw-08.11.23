#1.Write a Python program to multiply all the items in a list.
def mul_list_items(ls):
    for i in range(len(ls)):
        ls[i]*=3
    return ls
ls=[-5,10,20,0,100,1000]
print(mul_list_items(ls))

#2.Write a Python program to check if a list is empty or not.
def is_empty_list(ls1):
    if not ls1:                  #Version 1
        print("The list is empty")
    else:
        print("The list isn't empty")
    if (len(ls1)==0):             #Version 2
        print("The list is empty")
    else:
        print("The list isn't empty")
ls1=[1,2]
is_empty_list(ls1)  

# 3.Write a Python program to remove duplicates from a list.
def remove_list_dupl(list1):
    set1=set(list1)
    new_list=list(set1)
    return new_list
list1=[-5,4,'m',7,'v',4,'v']
print("The new list-> ",remove_list_dupl(list1))

#4. Write a Python program to convert a list of characters into a string.
def list_to_string(list):
   string="".join(list)
   return string
list2=['a','b','c']
print(f"From {list2} list to '{list_to_string(list2)}' string")

#5. Write a Python program to print the number of specified element in list.
def list_elem_index(list,elem):   #Version 1                      
    for i in range(len(list)):
        if list[i]==elem:
            return i
    
list3=[1,-5,'c',3.5,7]
elem=3.5
print(f"The {elem} element is at {list_elem_index(list3,elem)} index")
                                                         
index=list3.index(7)             #Version 2
print(index)

#6.  Write a Python program to remove the specified element.
def remove_list_elem(list, elem): #Version 1   
    new_list=[]
    for i in list:
        if i==elem:
            continue
        else:
            new_list.append(i)
    return new_list        
list4=[0,2,'f',-5,10]
elem=0
print("The new list is->",remove_list_elem(list4,elem))

list4.remove('f')                 #Version 2              
print("The new list is->",list4) 

# 7.Write a Python program to remove an element from the list and print it.
def return_removed_elem(list5, elem5):  
    new_list=[]
    for i in list5:
        if i!=elem5:
            new_list.append(i)
        else:
            print("The removed element is: ",i)
       
list5=[0,2,'f',-5,10]
elem5=-5
return_removed_elem(list5,elem5) 

#8. Write a Python program to insert a '&' before each element of a list.
list6=[1,-5,'k',7,'a']          
new_list=[]
for i in list6:                       # Version 1
    new_list.append(str(i)+'&')
print(new_list) 
new_list1=[str(i)+'&' for i in list6] # Version 2  /with list comprehension
print(new_list1)   

#9. Write a Python program to convert a list of multiple integers into a single integer.
    #Sample list: [11, 33, 50]  Expected Output: 113350
list7=[11, 33, 50]
list_to_str="".join([str(elem) for elem in list7])
str_to_int=int(list_to_str)
print(f"From {list7} list to {str_to_int} integer")

#10. Write a Python program to find all the values in a list that are greater than a specified number.
def grater_elem(list, elem):
    for i in list8:
        if(i>elem):
            print(i)
list8=[-1,0,7,9,100,-20,30]
elem=10
print(f"The elements greater than {elem} are: ")
grater_elem(list8,elem)

# 11. Write a Python function that takes two lists and returns True if they have at least one common member.
def check_lists(list1,list2):
    check=False
    for i in list1:
        for j in list2:
            if i==j:
                check=True
    return check
list9=[1,'a',-5,19]
list10=[20,7,1,100]
print(f"The {list9} and {list10} have at least one common member: ",check_lists(list9, list10))

#12. Write a Python program to insert a value into the sorted list of numbers leaving it sorted.
list11=[1,15,30,50,100]                        # Version 1
elem=70
list11.append(elem)
list11.sort()
print(list11)

list11_1=[1,15,30,50,100]                        # Version 2
elem_1=-5
for i in range (len(list11_1)):
    if(elem_1>list11_1[-1]):
            list11_1.append(elem_1)
    elif (elem_1>list11_1[i]):
        continue
    else:
        list11_1.insert(i,elem_1)
        break
print("new list",list11_1)        


#13.Write a Python program to count the number of strings from a given list of strings. 
# The string length is 2 or more and the first and last characters are the same.

list12=['pip','d','gaining','area','vb','skies','']
count=0
for i in list12:
    if len(i)>=2 and i[0]==i[-1]:
        count+=1
print("The count of matching strings is: ",count)        

